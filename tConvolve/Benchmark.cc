/// @copyright (c) 2007 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// This program was modified so as to use it in the contest.
/// The last modification was on January 12, 2015.
///

// Include own header file first
#include "Benchmark.h"

// System includes
#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <limits>
#include <omp.h>

Benchmark::Benchmark()
        : next(1)
{
}

// Return a pseudo-random integer in the range 0..2147483647
// Based on an algorithm in Kernighan & Ritchie, "The C Programming Language"
int Benchmark::randomInt()
{
    const unsigned int maxint = std::numeric_limits<int>::max();
    next = next * 1103515245 + 12345;
    return ((unsigned int)(next / 65536) % maxint);
}

void Benchmark::init()
{
    // Initialize the data to be gridded
    u.resize(nSamples);
    v.resize(nSamples);
    w.resize(nSamples);
    samples.resize(nSamples*nChan);
    outdata.resize(nSamples*nChan);


    Coord rd;
    FILE * fp;
    if( (fp=fopen("randnum.dat","rb"))==NULL )
    {
      printf("cannot open file\n");
      return;
    }
    
    for (int i = 0; i < nSamples; i++) {
        if(fread(&rd,sizeof(Coord),1,fp)!=1){printf("Rand number read error!\n");}
        u[i] = baseline * rd - baseline / 2;
        if(fread(&rd,sizeof(Coord),1,fp)!=1){printf("Rand number read error!\n");}
        v[i] = baseline * rd - baseline / 2;
        if(fread(&rd,sizeof(Coord),1,fp)!=1){printf("Rand number read error!\n");}
        w[i] = baseline * rd - baseline / 2;

        for (int chan = 0; chan < nChan; chan++) {
            if(fread(&rd,sizeof(Coord),1,fp)!=1){printf("Rand number read error!\n");}
            samples[i*nChan+chan].data.real = rd;
            outdata[i*nChan+chan].real = 0.0;
        }
    }
    fclose(fp);

    grid.resize(gSize*gSize);
    //grid.assign(grid.size(), Value(0.0));

    // Measure frequency in inverse wavelengths
    std::vector<Coord> freq(nChan);

    for (int i = 0; i < nChan; i++) {
        freq[i] = (1.4e9 - 2.0e5 * Coord(i) / Coord(nChan)) / 2.998e8;
    }

	char *cpuThreadStr = getenv("GRID_CPU_THREADS");
	if (cpuThreadStr != NULL)
    	std::stringstream(cpuThreadStr) >> cpuNumThreads;
	else
		cpuNumThreads = 16;

	char *micThreadStr = getenv("GRID_MIC_THREADS");
	if (micThreadStr != NULL)
    	std::stringstream(micThreadStr) >> micNumThreads;
	else
		micNumThreads = 240;

	char *workGraStr = getenv("GRID_WORK_GRANULARITY");
	if (workGraStr != NULL)
    	std::stringstream(workGraStr) >> workGranularity;
	else
		workGranularity = 32;

	std::cout << "cpuNumThreads=" << cpuNumThreads << "\n";
	std::cout << "micNumThreads=" << micNumThreads << "\n";
	std::cout << "workGranularity=" << workGranularity << "\n";

    // Initialize convolution function and offsets
    initC(freq, cellSize, wSize, m_support, overSample, wCellSize, C);
    initCOffset(u, v, w, freq, cellSize, wCellSize, wSize, gSize,
                m_support, overSample);
}

void Benchmark::runGrid()
{
    gridKernel(m_support, C, grid, gSize);
}

/////////////////////////////////////////////////////////////////////////////////
// The next function is the kernel of the gridding.
// The data are presented as a vector. Offsets for the convolution function
// and for the grid location are precalculated so that the kernel does
// not need to know anything about world coordinates or the shape of
// the convolution function. The ordering of cOffset and iu, iv is
// random.
//
// Perform gridding
//
// data - values to be gridded in a 1D vector
// support - Total width of convolution function=2*support+1
// C - convolution function shape: (2*support+1, 2*support+1, *)
// cOffset - offset into convolution function per data point
// iu, iv - integer locations of grid points
// grid - Output grid: shape (gSize, *)
// gSize - size of one axis of grid
#if 0
void Benchmark::gridKernel(const int support,
                           const std::vector<Value>& C,
                           std::vector<Value>& grid, const int gSize)
{
    const int sSize = 2 * support + 1;
	//omp_set_num_threads(16);

	int grid_size = grid.size();
	int samples_size = samples.size();
	int C_size = C.size();

	Sample *samples_array = (Sample*) malloc(sizeof(Sample) * samples.size());
	Value *grid_array = (Value*) malloc(sizeof(Value) * grid.size());
	Value *C_array = (Value*) malloc(sizeof(Value) * C.size());

	copy(samples.begin(), samples.end(), samples_array);
	copy(C.begin(), C.end(), C_array);

#pragma offload target(mic) in(C_array:length(C_size)) \
	in(samples_array:length(samples_size)) \
	out(grid_array:length(grid_size))
{
	#pragma omp parallel num_threads(60)
	{
	Value *local_grid = NULL;
	int tid = omp_get_thread_num();
	if (tid < 10) local_grid = (Value*) malloc(sizeof(Value) * grid_size);

	#pragma omp for nowait
    for (int dind = 0; dind < int(samples_size) * 6; ++dind) {
		if (dind < int(samples_size)) {
        	// The actual grid point from which we offset
        	int gind = samples_array[dind].iu + gSize * samples_array[dind].iv - support;
		
			// The Convoluton function point from which we offset
        	int cind = samples_array[dind].cOffset;
		
        	for (int suppv = 0; suppv < sSize; suppv++) {
            	#pragma omp task
				{
				Value* gptr = &local_grid[gind];
            	Value* cptr = &C_array[cind];
            	Value d = samples_array[dind].data;
				for (int suppu = 0; suppu < sSize; suppu++) {
              		(*(gptr++)).real += d.real * (*(cptr++)).real;
            	}

            	gind += gSize;
            	cind += sSize;
				} /* end omp task */
        	}
			#pragma omp taskwait
		}
    }
	if (tid < 10) {
		#pragma omp critical
		{
			Value* gptr = &grid_array[0];
			const Value* lgptr = &local_grid[0];
		
			for (int i = 0; i < grid_size; i++)
				(*(gptr++)).real += (*(lgptr++)).real;
		}
		free(local_grid);
	}
	}
} /* end pragma offload */

	copy(grid_array, grid_array + grid_size, grid.begin());

	free(C_array);
	free(grid_array);
	free(samples_array);
}
#endif

/////////////////////////////////////////////////////////////////////////////////
// Initialize W project convolution function
// - This is application specific and should not need any changes.
//
// freq - temporal frequency (inverse wavelengths)
// cellSize - size of one grid cell in wavelengths
// wSize - Size of lookup table in w
// support - Total width of convolution function=2*support+1
// wCellSize - size of one w grid cell in wavelengths
void Benchmark::initC(const std::vector<Coord>& freq,
                      const Coord cellSize, const int wSize,
                      int& support, int& overSample,
                      Coord& wCellSize, std::vector<Value>& C)
{
    std::cout << "Initializing W projection convolution function" << std::endl;
    support = static_cast<int>(1.5 * sqrt(std::abs(baseline) * static_cast<Coord>(cellSize)
                                          * freq[0]) / cellSize);

    overSample = 8;
    std::cout << "Support = " << support << " pixels" << std::endl;
    wCellSize = 2 * baseline * freq[0] / wSize;
    std::cout << "W cellsize = " << wCellSize << " wavelengths" << std::endl;

    // Convolution function. This should be the convolution of the
    // w projection kernel (the Fresnel term) with the convolution
    // function used in the standard case. The latter is needed to
    // suppress aliasing. In practice, we calculate entire function
    // by Fourier transformation. Here we take an approximation that
    // is good enough.
    const int sSize = 2 * support + 1;

    const int cCenter = (sSize - 1) / 2;

    C.resize(sSize*sSize*overSample*overSample*wSize);
    std::cout << "Size of convolution function = " << sSize*sSize*overSample
              *overSample*wSize*sizeof(Value) / (1024*1024) << " MB" << std::endl;
    std::cout << "Shape of convolution function = [" << sSize << ", " << sSize << ", "
                  << overSample << ", " << overSample << ", " << wSize << "]" << std::endl;

    for (int k = 0; k < wSize; k++) {
        double w = double(k - wSize / 2);
        double fScale = sqrt(std::abs(w) * wCellSize * freq[0]) / cellSize;

        for (int osj = 0; osj < overSample; osj++) {
            for (int osi = 0; osi < overSample; osi++) {
                for (int j = 0; j < sSize; j++) {
                    double j2 = std::pow((double(j - cCenter) + double(osj) / double(overSample)), 2);

                    for (int i = 0; i < sSize; i++) {
                        double r2 = j2 + std::pow((double(i - cCenter) + double(osi) / double(overSample)), 2);
                        long int cind = i + sSize * (j + sSize * (osi + overSample * (osj + overSample * k)));

                        if (w != 0.0) {
                            C[cind].real = std::cos(r2 / (w * fScale));
                        } else {
                            C[cind].real = std::exp(-r2);
                        }
                    }
                }
            }
        }
    }

    // Now normalise the convolution function
    Coord sumC = 0.0;

    for (int i = 0; i < sSize*sSize*overSample*overSample*wSize; i++) {
        sumC += sqrt((C[i].real*C[i].real) + (C[i].imag*C[i].imag));
    }

	// Vector Abs
	//vzAbs( sSize*sSize*overSample*overSample*wSize, &C, &sumC);

    for (int i = 0; i < sSize*sSize*overSample*overSample*wSize; i++) {
        // C[i] *= Value(wSize * overSample * overSample / sumC);
        C[i].real *= (wSize * overSample * overSample / sumC);
    }
}

// Initialize Lookup function
// - This is application specific and should not need any changes.
//
// freq - temporal frequency (inverse wavelengths)
// cellSize - size of one grid cell in wavelengths
// gSize - size of grid in pixels (per axis)
// support - Total width of convolution function=2*support+1
// wCellSize - size of one w grid cell in wavelengths
// wSize - Size of lookup table in w
void Benchmark::initCOffset(const std::vector<Coord>& u, const std::vector<Coord>& v,
                            const std::vector<Coord>& w, const std::vector<Coord>& freq,
                            const Coord cellSize, const Coord wCellSize,
                            const int wSize, const int gSize, const int support,
                            const int overSample)
{
    const int nSamples = u.size();
    const int nChan = freq.size();

    const int sSize = 2 * support + 1;

    // Now calculate the offset for each visibility point
    for (int i = 0; i < nSamples; i++) {
        for (int chan = 0; chan < nChan; chan++) {

            int dind = i * nChan + chan;

            Coord uScaled = freq[chan] * u[i] / cellSize;
            samples[dind].iu = int(uScaled);

            if (uScaled < Coord(samples[dind].iu)) {
                samples[dind].iu -= 1;
            }

            int fracu = int(overSample * (uScaled - Coord(samples[dind].iu)));
            samples[dind].iu += gSize / 2;

            Coord vScaled = freq[chan] * v[i] / cellSize;
            samples[dind].iv = int(vScaled);

            if (vScaled < Coord(samples[dind].iv)) {
                samples[dind].iv -= 1;
            }

            int fracv = int(overSample * (vScaled - Coord(samples[dind].iv)));
            samples[dind].iv += gSize / 2;

            // The beginning of the convolution function for this point
            Coord wScaled = freq[chan] * w[i] / wCellSize;
            int woff = wSize / 2 + int(wScaled);
            samples[dind].cOffset = sSize * sSize * (fracu + overSample * (fracv + overSample * woff));
        }
    }
}

void Benchmark::gridKernelMic(const int support,
                           const std::vector<Value>& C,
                           std::vector<Value>& grid, const int gSize, const int samples_size)
{
    const int sSize = 2 * support + 1;

    int grid_size = grid.size();
    int C_size = C.size();

    Sample *samples_array = (Sample*) malloc(sizeof(Sample) * samples_size);
    Value *grid_array = (Value*) malloc(sizeof(Value) * grid.size());
    Value *C_array = (Value*) malloc(sizeof(Value) * C.size());

    copy(samples.begin(), samples.begin() + samples_size, samples_array);
    copy(C.begin(), C.end(), C_array);

#pragma offload target(mic) in(C_array:length(C_size)) \
        in(samples_array:length(samples_size)) \
        out(grid_array:length(grid_size))
{
	#pragma omp parallel for num_threads(micNumThreads) \
		schedule(dynamic, workGranularity)
    for (int dind = 0; dind < int(samples_size); ++dind) {
        // The actual grid point from which we offset
        int gind = samples_array[dind].iu + gSize * samples_array[dind].iv - support;
                
        // The Convoluton function point from which we offset
        int cind = samples_array[dind].cOffset;
                
        for (int suppv = 0; suppv < sSize; ++suppv) {
            Value* gptr = &grid_array[gind];
            Value* cptr = &C_array[cind];
            Value d = samples_array[dind].data;
            Value tmp;
			#pragma unroll(16)
			for (int suppu = 0; suppu < sSize; ++suppu) {
				tmp.real = d.real * (*(cptr++)).real;
				#pragma omp atomic
				(*(gptr++)).real += tmp.real; 
            }

            gind += gSize;
            cind += sSize;
        }
    }
} /* end pragma offload */

	//#pragma omp critical
	{
    	Value* gptr = &grid[0];
    	const Value* lgptr = &grid_array[0];

    	#pragma unroll(16)
    	for (int i = 0; i < grid_size; ++i) {
       		#pragma omp atomic
			(*(gptr++)).real += (*(lgptr++)).real;
		}
	}

    free(C_array);
    free(grid_array);
    free(samples_array);
}

void Benchmark::gridKernelCpu(const int support,
                              const std::vector<Value>& C,
							  std::vector<Value>& grid,
                              std::vector<Value>& local_grid, const int gSize,
                              int cpuStartSample)
{
    const int sSize = 2 * support + 1;
	std::vector<Value> grids[cpuNumThreads];

#pragma omp parallel num_threads(cpuNumThreads)
{
	int tid = omp_get_thread_num();
	std::vector<Value> & nested_local_grid = grids[tid];
	nested_local_grid.resize(grid.size());

    #pragma omp for schedule(dynamic, workGranularity)
    for (int dind = cpuStartSample; dind < int(samples.size()); ++dind) {
        /* The actual grid point from which we offset */
        int gind = samples[dind].iu + gSize * samples[dind].iv - support;

        /* The Convoluton function point from which we offset */
        int cind = samples[dind].cOffset;

        for (int suppv = 0; suppv < sSize; suppv++) {
            const Value d = samples[dind].data;
			#pragma simd
            for (int suppu = 0; suppu < sSize; suppu++) {
				nested_local_grid[gind+suppu].real += d.real * C[cind+suppu].real;
            }
            
			gind += gSize;
            cind += sSize;
        }
    } /* end omp for */
	#pragma omp for schedule(static, 1)
	for (int i = 0; i < local_grid.size(); ++i) {
		#pragma simd
		for (int k = 0; k < cpuNumThreads; ++k) {
			local_grid[i].real += grids[k][i].real;
		}
	}
} /* end omp parallel */

	//#pragma omp critical
	{
    	Value* gptr = &grid[0];
    	const Value* lgptr = &local_grid[0];

    	#pragma unroll(16)
    	for (int i = 0; i < grid.size(); i++) {
        	#pragma omp atomic
			(*(gptr++)).real += (*(lgptr++)).real;
		}
	}
}

void Benchmark::gridKernel(const int support,
                           const std::vector<Value>& C,
                           std::vector<Value>& grid, const int gSize)
{
    omp_set_nested(1);
    omp_set_dynamic(1);

    /* TODO: how to adjust this factor */
    float splitRatio = 0.51;  /* MIC 27%, CPU 73% */
    int cpuStartSample = ((samples.size() * splitRatio) / workGranularity) * workGranularity;
    int micSampleSize = cpuStartSample;

	#pragma omp parallel sections num_threads(2)
	{
    #pragma omp section
    {
		std::vector<Value> local_grid(grid.size());
	    gridKernelCpu(support, C, grid, local_grid, gSize, cpuStartSample);
		//std::cout << "CPU Finish\n";
	}
	#pragma omp section
	{
        gridKernelMic(support, C, grid, gSize, micSampleSize);
		//std::cout << "MIC Finish\n";
    }
	} /* end omp parallel */
}


void Benchmark::printGrid()
{
  FILE * fp;
  if( (fp=fopen("grid.dat","wb"))==NULL )
  {
    printf("cannot open file\n");
    return;
  }  

  unsigned ij;
  for (int i = 0; i < gSize; i++)
  {
    for (int j = 0; j < gSize; j++)
    {
      ij=j+i*gSize;
      if(fwrite(&grid[ij],sizeof(Value),1,fp)!=1)
        printf("File write error!\n"); 

    }
  }
  
  fclose(fp);
}

int Benchmark::getSupport()
{
    return m_support;
};
